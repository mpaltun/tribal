﻿using System;
using TribalGame.Core;
using Xunit;

namespace TribalGame.Tests
{
    public class GameTests
    {
        [Fact]
        public void HuntReturnsCorrectInventory()
        {
            var player = newPlayer("username", "passwd", newInventory(0, 0, 4, 4));

            var seed = 20;
            // this seed causes:
            //  collect 0 + 2 + 0 + 3 = 5 pelts
            //  1 member gets hurt

            Game game = new Game(player, new Random(seed));
            game.Hunt();

            Assert.Equal(3, player.Inventory.HealthyMembers);
            Assert.Equal(5, player.Inventory.Pelts);
        }


        [Fact]
        public void HuntNeedsHealthyMembers()
        {
            var player = newPlayer("username", "passwd", newInventory(1, 0, 0, 4));

            Game game = new Game(player);
            game.Hunt();

            Assert.Equal(1, player.Inventory.Pelts);
            Assert.Equal(0, player.Inventory.HealthyMembers);
        }

        [Fact]
        public void HealDoesNothingIfMammothTeethAreInsufficient()
        {
            var player = newPlayer("username", "passwd", newInventory(10, 1, 0, 4));
            var game = new Game(player);
            game.Heal();

            Assert.Equal(newInventory(10, 1, 0, 4), player.Inventory);
        }

        [Fact]
        public void HealSuccessIfMammothTeethAreSufficient()
        {
            var player = newPlayer("username", "passwd", newInventory(10, 5, 0, 4));

            var game = new Game(player);
            game.Heal();

            Assert.Equal(3, player.Inventory.MammothsTeeth);
            Assert.Equal(1, player.Inventory.HealthyMembers);
        }

        [Fact]
        public void HealDoesNothingIfEveryoneIsHealthy()
        {
            var player = newPlayer("username", "passwd", newInventory(10, 3, 4, 4));

            var game = new Game(player);
            game.Heal();

            Assert.Equal(newInventory(10, 3, 4, 4), player.Inventory);
        }

        [Fact]
        public void TradeDoesNothingIfThereAreNotEnoughPelts()
        {
            var player = newPlayer("username", "passwd", newInventory(4, 1, 0, 4));
            var game = new Game(player);
            game.Trade();

            Assert.Equal(newInventory(4, 1, 0, 4), player.Inventory);
        }

        [Fact]
        public void TradeSuccessIfThereAreEnoughPelts()
        {
            var player = newPlayer("username", "passwd", newInventory(11, 1, 0, 4));
            var game = new Game(player);
            game.Trade();

            Assert.Equal(6, player.Inventory.Pelts);
            Assert.Equal(2, player.Inventory.MammothsTeeth);
        }

        private Inventory newInventory(int pelts, int mammothsTeeth, int healthyMembers, int totalMembers)
        {
            return new Inventory
            {
                Pelts = pelts,
                MammothsTeeth = mammothsTeeth,
                HealthyMembers = healthyMembers,
                TotalMembers = totalMembers
            };
        }

        private Player newPlayer(string username, string password, Inventory inventory)
        {
            return new Player
            {
                Username = username,
                Password = password,
                Inventory = inventory
            };
        }
    }
}