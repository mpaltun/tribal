using MongoDB.Driver;
using TribalGame.Core;
using TribalGame.Server;
using Xunit;
using static TribalGame.Core.LoginResponse.Types;

namespace TribalGame.Tests
{
    public class PlayerServiceTests
    {
        private readonly PlayerService playerService;

        public PlayerServiceTests()
        {
            var mongoClient = new MongoClient("mongodb://127.0.0.1:27017");
            mongoClient.DropDatabase("TribalTest");

            this.playerService = new MongoDbPlayerService(mongoClient, "TribalTest");
        }

        [Fact]
        public void LoginReturnsInvalidArgsIfNullUsernamePassed()
        {
            (Player _, LoginError error) = playerService.Login(null, "password");
            Assert.Equal(LoginError.InvalidUsername, error);
        }

        [Fact]
        public void LoginReturnsInvalidArgsIfBlankUsernamePassed()
        {
            (Player _, LoginError error) = playerService.Login(" ", "password");
            Assert.Equal(LoginError.InvalidUsername, error);
        }

        [Fact]
        public void LoginReturnsInvalidArgsIfNullPasswordPassed()
        {
            (Player _, LoginError error) = playerService.Login("username", null);
            Assert.Equal(LoginError.InvalidPassword, error);
        }

        [Fact]
        public void LoginReturnsInvalidArgsIfBlankPasswordPassed()
        {
            (Player _, LoginError error) = playerService.Login("username", " ");
            Assert.Equal(LoginError.InvalidPassword, error);
        }

        [Fact]
        public void LoginSucceedsWithValidArguments()
        {
            (Player player, LoginError error) = playerService.Login("username", "password");
            Assert.Equal(LoginError.None, error);
            Assert.Equal(player.Username, "username");
            Assert.Equal(player.Password, "password");
        }

        [Fact]
        public void FirstTimeLoginRegistersPlayerWithDefaultInventory()
        {
            (Player player, LoginError error) = playerService.Login("username", "password");
            Assert.Equal(LoginError.None, error);

            Assert.Equal(player.Inventory.HealthyMembers, 16);
            Assert.Equal(player.Inventory.TotalMembers, 16);
            Assert.Equal(player.Inventory.MammothsTeeth, 0);
            Assert.Equal(player.Inventory.Pelts, 0);
        }

        [Fact]
        public void LoginSuccessForAlreadyRegisteredPlayer()
        {
            (Player player, LoginError error) = playerService.Login("username", "password");
            Assert.Equal(LoginError.None, error);
            Assert.NotNull(player);

            (Player secondLoginPlayer, LoginError secondLoginError) = playerService.Login("username", "password");
            Assert.Equal(LoginError.None, secondLoginError);
            Assert.NotNull(secondLoginPlayer);

            Assert.Equal(player, secondLoginPlayer);
        }

        [Fact]
        public void LoginReturnsErrorIfPasswordMismatches()
        {
            (Player _, LoginError error) = playerService.Login("username", "passwd");
            Assert.Equal(LoginError.None, error);

            (Player _, LoginError error2) = playerService.Login("username", "password");
            Assert.Equal(LoginError.PasswordMismatch, error2);
        }

        [Fact]
        public void LoginReturnsDetachedPlayer()
        {
            (Player player, LoginError _) = playerService.Login("username", "password");

            player.Inventory = new Inventory
            {
                Pelts = 0,
                MammothsTeeth = 0,
                HealthyMembers = 0,
                TotalMembers = 0
            };

            (Player secondLoginPlayer, LoginError _) = playerService.Login("username", "password");
            Assert.NotEqual(player, secondLoginPlayer);
        }
    }
}