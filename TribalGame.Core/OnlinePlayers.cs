﻿namespace TribalGame.Core
{
    public interface OnlinePlayers
    {
        Player SetOnline(Player player);

        PlayerSession Find(string username);
    }
}