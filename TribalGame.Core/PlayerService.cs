﻿namespace TribalGame.Core
{
    public interface PlayerService
    {
        (Player player, LoginResponse.Types.LoginError error) Login(string username, string password);

        void Save(Player player);
    }
}