﻿using System;

namespace TribalGame.Core
{
    public class Game
    {
        private readonly Random random;
        private Player player;

        public Game(Player player) : this(player, new Random())
        {
        }

        public Game(Player player, Random random)
        {
            this.player = player;
            this.random = random;
        }

        public void Play(GameAction action)
        {
            switch (action)
            {
                case GameAction.Hunt:
                    Hunt();
                    break;
                case GameAction.Heal:
                    Heal();
                    break;
                case GameAction.Trade:
                    Trade();
                    break;
            }
        }

        public void Hunt()
        {
            var inventory = player.Inventory;
            int peltsAcquired = 0, injuredMembers = 0;
            for (int i = 0; i < inventory.HealthyMembers; i++)
            {
                peltsAcquired += random.Next(0, 4);
                // 5% chance to get hurt
                if (random.Next(0, 100) < 5)
                {
                    injuredMembers++;
                }
            }

            if (peltsAcquired > 0 || injuredMembers > 0)
            {
                player.Inventory = new Inventory
                {
                    Pelts = inventory.Pelts + peltsAcquired,
                    MammothsTeeth = inventory.MammothsTeeth,
                    HealthyMembers = inventory.HealthyMembers - injuredMembers,
                    TotalMembers = inventory.TotalMembers
                };
            }
        }

        public void Heal()
        {
            var inventory = player.Inventory;
            if (inventory.MammothsTeeth < 2)
            {
                return;
            }

            if (inventory.HealthyMembers < inventory.TotalMembers)
            {
                player.Inventory = new Inventory
                {
                    Pelts = inventory.Pelts,
                    MammothsTeeth = inventory.MammothsTeeth - 2,
                    HealthyMembers = inventory.HealthyMembers + 1,
                    TotalMembers = inventory.TotalMembers
                };
            }
        }

        public void Trade()
        {
            var inventory = player.Inventory;
            if (inventory.Pelts < 5)
            {
                return;
            }

            player.Inventory = new Inventory
            {
                Pelts = inventory.Pelts - 5,
                MammothsTeeth = inventory.MammothsTeeth + 1,
                HealthyMembers = inventory.HealthyMembers,
                TotalMembers = inventory.TotalMembers
            };
        }
    }
}