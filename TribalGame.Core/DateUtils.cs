using System;

namespace TribalGame.Core
{
    public class DateUtils
    {
        public static long EpochSeconds()
        {
            return (long) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}