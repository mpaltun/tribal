﻿using System;

namespace TribalGame.Core
{
    public interface PlayerSession
    {
        Player Player();

        Random Random();
    }
}