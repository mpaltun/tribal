setlocal

cd /d %~dp0
                         
set PROTOC=%UserProfile%\.nuget\packages\google.protobuf.tools\3.3.0\tools\windows_x64\protoc.exe
set PLUGIN=%UserProfile%\.nuget\packages\grpc.tools\1.4.1\tools\windows_x64\grpc_csharp_plugin.exe

%PROTOC% -I proto --csharp_out gen proto/tribal.proto --grpc_out gen --plugin=protoc-gen-grpc=%PLUGIN%

endlocal