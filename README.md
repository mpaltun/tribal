# README #

Tribal Game

### How do I run the client ? ###
* git clone git@bitbucket.org:mpaltun/tribal.git
* cd tribal
* dotnet restore
* dotnet build
* dotnet publish -c Release
* dotnet TribalGame\bin\Release\netcoreapp1.1\TribalGame.dll

### Missing pieces ###

* More tests
* Auto heal from server side
* Set offline players on server