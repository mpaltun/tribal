using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Protobuf.Collections;
using Grpc.Core;
using TribalGame.Core;
using static TribalGame.Core.GameActionsResponse.Types;

namespace TribalGame.Server
{
    class TribalServerApi : Tribal.TribalBase
    {
        private readonly PlayerService playerService;
        private readonly OnlinePlayers onlinePlayers;

        public TribalServerApi(PlayerService playerService, OnlinePlayers onlinePlayers)
        {
            this.playerService = playerService;
            this.onlinePlayers = onlinePlayers;
        }

        public override Task<LoginResponse> Login(LoginRequest request, ServerCallContext context)
        {
            (Player player, LoginResponse.Types.LoginError error) = playerService.Login(request.Username, request.Password);
            if (player != null)
            {
                player = onlinePlayers.SetOnline(player);
            }
            return Task.FromResult(new LoginResponse {Player = player, Error = error});
        }

        public override async Task play(IAsyncStreamReader<GameActionsRequest> requestStream,
            IServerStreamWriter<GameActionsResponse> responseStream, ServerCallContext context)
        {
            while (await requestStream.MoveNext())
            {
                var request = requestStream.Current;

                var session = onlinePlayers.Find(request.Player.Username);
                if (session == null || !request.Player.SessionId.Equals(session.Player().SessionId))
                {
                    await responseStream.WriteAsync(new GameActionsResponse
                    {
                        Error = GameActionsError.InvalidSession
                    });
                    break;
                }

                replay(session, request.Actions);

                var error = GameActionsError.None;
                if (!request.Player.Inventory.Equals(session.Player().Inventory))
                {
                    error = GameActionsError.InvalidState;
                }

                await responseStream.WriteAsync(new GameActionsResponse {Error = error});
                if (error != GameActionsError.None)
                {
                    break;
                }
            }
        }

        private void replay(PlayerSession session, RepeatedField<GameAction> actions)
        {
            var player = session.Player();
            var game = new Game(player, session.Random());
            foreach (var action in actions)
            {
                game.Play(action);
            }
            playerService.Save(player);
        }
    }
}