﻿using System;
using System.Collections.Concurrent;
using TribalGame.Core;

namespace TribalGame.Server
{
    public class InMemoryOnlinePlayers  : OnlinePlayers
    {
        private readonly ConcurrentDictionary<string, PlayerSession> map = new ConcurrentDictionary<string, PlayerSession>();

        public PlayerSession Find(string username)
        {
            PlayerSession session;
            map.TryGetValue(username, out session);

            return session;
        }

        public Player SetOnline(Player player)
        {
            player.SessionId = Guid.NewGuid().ToString();
            player.Seed = Environment.TickCount;

            var existing = map.GetOrAdd(player.Username, newSession(player));
            if (existing != null)
            {
                // inventory in memory might be up to date
                player.Inventory = existing.Player().Inventory;
                map[player.Username] = newSession(player);
            }
            return player;
        }

        private static DefaultPlayerSession newSession(Player player)
        {
            return new DefaultPlayerSession(player, new Random(player.Seed));
        }
    }
}