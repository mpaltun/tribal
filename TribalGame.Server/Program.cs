﻿using System;
using Grpc.Core;
using MongoDB.Driver;
using TribalGame.Core;

namespace TribalGame.Server
{
    class Program
    {
        const int Port = 8000;

        static void Main(string[] args)
        {
            var dbConnectionString = Environment.GetEnvironmentVariable("DB_URI") ?? "mongodb://127.0.0.1:27017";

            var mongoClient = new MongoClient(dbConnectionString);
            var playerService = new MongoDbPlayerService(mongoClient, "Tribal");
            var onlineUsers = new InMemoryOnlinePlayers();

            Grpc.Core.Server server = new Grpc.Core.Server
            {
                Services =
                {
                    Tribal.BindService(new TribalServerApi(playerService, onlineUsers))
                },
                Ports = {new ServerPort("0.0.0.0", Port, ServerCredentials.Insecure)}
            };
            server.Start();

            Console.WriteLine("Tribal game server listening on port " + Port);
            Console.WriteLine("Press Enter to terminate...");
            Console.ReadLine();

            server.ShutdownAsync().Wait();
        }
    }
}