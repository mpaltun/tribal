﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;
using TribalGame.Core;
using static TribalGame.Core.LoginResponse.Types;

namespace TribalGame.Server
{
    public class MongoDbPlayerService : PlayerService
    {
        private readonly IMongoCollection<BsonDocument> collection;

        public MongoDbPlayerService(MongoClient client, string database)
        {
            var db = client.GetDatabase(database);

            collection = db.GetCollection<BsonDocument>("players");
            collection.Indexes.CreateOne("{ username : 1 }", new CreateIndexOptions {Unique = true});
        }

        public (Player player, LoginError error) Login(string username, string password)
        {
            if (String.IsNullOrWhiteSpace(username))
            {
                return (null, LoginError.InvalidUsername);
            }

            if (String.IsNullOrWhiteSpace(password))
            {
                return (null, LoginError.InvalidPassword);
            }

            var filter = Builders<BsonDocument>.Filter.Eq("username", username);
            var document = collection.Find(filter).FirstOrDefault();
            if (document == null)
            {
                return register(username, password);
            }

            if (password.Equals(document["password"].AsString))
            {
                return (playerFromDoc(document), LoginError.None);
            }
            return (null, LoginError.PasswordMismatch);
        }

        public void Save(Player player)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("username", player.Username);
            var update = Builders<BsonDocument>.Update.Set("inventory", new BsonDocument
            {
                {"pelts", player.Inventory.Pelts},
                {"mammothsTeeth", player.Inventory.MammothsTeeth},
                {"healthyMembers", player.Inventory.HealthyMembers},
                {"totalMembers", player.Inventory.TotalMembers}
            });
            collection.UpdateOneAsync(filter, update);
        }

        private (Player player, LoginError error) register(string username, string password)
        {
            var document = new BsonDocument
            {
                {"username", username},
                // I know I know.. I shouldn't store plain passwords in database
                {"password", password},
                {
                    "inventory", new BsonDocument
                    {
                        {"pelts", 0},
                        {"mammothsTeeth", 0},
                        {"healthyMembers", 16},
                        {"totalMembers", 16}
                    }
                }
            };

            bool registered = true;
            try
            {
                collection.InsertOne(document);
            }
            catch (Exception)
            {
                registered = false;
            }

            if (registered)
            {
                return (playerFromDoc(document), LoginError.None);
            }
            return (null, LoginError.PasswordMismatch);
        }

        private Player playerFromDoc(BsonDocument document)
        {
            return new Player()
            {
                Username = document["username"].AsString,
                Password = document["password"].AsString,
                Inventory = new Inventory
                {
                    Pelts = document["inventory"]["pelts"].AsInt32,
                    MammothsTeeth = document["inventory"]["mammothsTeeth"].AsInt32,
                    HealthyMembers = document["inventory"]["healthyMembers"].AsInt32,
                    TotalMembers = document["inventory"]["totalMembers"].AsInt32
                }
            };
        }
    }
}