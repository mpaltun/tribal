﻿using System;
using TribalGame.Core;

namespace TribalGame.Server
{
    public class DefaultPlayerSession : PlayerSession
    {
        public DefaultPlayerSession(Player player, Random random)
        {
            this.player = player;
            this.random = random;
        }

        private readonly Player player;
        private readonly Random random;

        Player PlayerSession.Player()
        {
            return this.player;
        }

        Random PlayerSession.Random()
        {
            return this.random;
        }
    }
}