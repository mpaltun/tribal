﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using TribalGame.Core;
using static TribalGame.Core.GameActionsResponse.Types;

namespace TribalGame
{
    internal class TribalGameConsoleApp
    {
        private readonly ConcurrentQueue<GameActionsResponse> serverEvents = new ConcurrentQueue<GameActionsResponse>();
        private readonly Tribal.TribalClient tribalClient;

        private volatile AsyncDuplexStreamingCall<GameActionsRequest, GameActionsResponse> playRpc;

        private Player player;
        private Queue<GameAction> outgoingActions = new Queue<GameAction>(5);

        public TribalGameConsoleApp(Tribal.TribalClient tribalClient)
        {
            this.tribalClient = tribalClient;
        }

        public void Init()
        {
            Console.WriteLine("== Tribal Game ==");
        }

        public void Run()
        {
            player = login();
            gameLoop();
        }

        private void printUsage()
        {
            Console.WriteLine();
            Console.WriteLine("Press");
            Console.WriteLine();
            Console.WriteLine("* 1 to hunt.");
            Console.WriteLine("* 2 to heal one member in return of two mammoth teeth");
            Console.WriteLine("* 3 to buy one mammoth tooth");
            Console.WriteLine();
            Console.WriteLine("* 9 to add 10 pelts instantly");
            Console.WriteLine("* 0 to exit");
        }

        private void gameLoop()
        {
            var game = new Game(player, new Random(player.Seed));
            while (true)
            {
                Console.WriteLine();
                printInventory();
                printUsage();

                processUserInputs(game);
                var error = processServerEvents();
                if (error)
                {
                    player = login();
                }
            }
        }

        private bool processServerEvents()
        {
            while (serverEvents.TryDequeue(out GameActionsResponse response))
            {
                var error = response.Error;
                if (error != GameActionsError.None)
                {
                    if (error == GameActionsError.InvalidState)
                    {
                        Console.WriteLine("Are you doing bad things ?");
                    }
                    else if (error == GameActionsError.InvalidSession)
                    {
                        Console.WriteLine("Session closed by server");
                    }

                    return true;
                }
            }
            return false;
        }

        private void processUserInputs(Game game)
        {
            var action = readAction();
            if (action != GameAction.None)
            {
                game.Play(action);
                sendActionsToServer(action);
            }
        }

        private void printInventory()
        {
            Console.WriteLine($"Pelts: {player.Inventory.Pelts}, MammothsTeeth: {player.Inventory.MammothsTeeth}, " +
                              $"Healthy: {player.Inventory.HealthyMembers}, Total: {player.Inventory.TotalMembers}");
        }

        private void sendActionsToServer(GameAction action)
        {
            outgoingActions.Enqueue(action);
            if (outgoingActions.Count == 5)
            {
                sendPendingActions();
            }
        }

        private void sendPendingActions()
        {
            // TODO: handle errors
            playRpc.RequestStream.WriteAsync(new GameActionsRequest
            {
                Actions = {outgoingActions},
                Player = player
            });

            outgoingActions.Clear();
        }

        private GameAction readAction()
        {
            if (int.TryParse(Console.ReadKey().KeyChar.ToString(), out int input))
            {
                // check exit
                if (input == 0)
                {
                    sendPendingActions();
                    playRpc.RequestStream.CompleteAsync();
                    Console.WriteLine("Bye!");
                    Environment.Exit(0);
                }

                if (Enum.IsDefined(typeof(GameAction), input))
                {
                    return (GameAction) input;
                }

                if (input == 9)
                {
                    // cheat
                    player.Inventory.Pelts += 10;
                }
            }
            return GameAction.None;
        }

        private Player login()
        {
            Player player;
            while (true)
            {
                Console.WriteLine("Enter Username:");
                var username = Console.ReadLine();

                Console.WriteLine("Enter Password:");
                // TODO: Hide password
                var password = Console.ReadLine();

                LoginResponse loginResult;
                try
                {
                    loginResult = tribalClient.Login(new LoginRequest {Username = username, Password = password});
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Environment.Exit(1);
                    return null;
                }

                if (loginResult.Player != null)
                {
                    Console.WriteLine($"Welcome {loginResult.Player.Username}!");
                    player = loginResult.Player;
                    break;
                }
                Console.WriteLine($"Login failed. Reason: {loginResult.Error}");
            }

            postLogin();

            return player;
        }

        private void postLogin()
        {
            if (playRpc != null)
            {
                playRpc.RequestStream.CompleteAsync();
                playRpc.Dispose();
            }

            playRpc = tribalClient.play();

            // listen server responses and push to serverEvents queue
            Task.Run(async () =>
            {
                while (await playRpc.ResponseStream.MoveNext())
                {
                    var response = playRpc.ResponseStream.Current;
                    serverEvents.Enqueue(response);
                }
            });
        }
    }
}