﻿using System;
using Grpc.Core;
using TribalGame.Core;

namespace TribalGame
{
    class Program
    {
        static void Main(string[] args)
        {
            var backendAddress = Environment.GetEnvironmentVariable("TRIBAL_BACKEND") ?? "tribal-lb2-6165167.us-west-2.elb.amazonaws.com:8000";

            Channel channel = new Channel(backendAddress, ChannelCredentials.Insecure);
            var tribalClient = new Tribal.TribalClient(channel);

            var app = new TribalGameConsoleApp(tribalClient);
            app.Init();
            app.Run();
        }
    }
}